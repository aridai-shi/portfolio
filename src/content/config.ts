import { z, defineCollection } from 'astro:content';
const projectCollection = defineCollection({
    type: 'content', // v2.5.0 and later
    schema: z.object({
      title: z.string(),
      tags: z.array(z.string()),
      image: z.string().optional(),
      description: z.string(),
      link: z.string().optional(),
      short: z.boolean().optional().default(false),
      masonry: z.boolean().optional().default(false)
    }),
  });
  
  
export const collections = {
  'projects': projectCollection,
};