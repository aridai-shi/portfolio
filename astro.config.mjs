import { defineConfig } from 'astro/config';
import mdx from "@astrojs/mdx";

import sitemap from "@astrojs/sitemap";

// https://astro.build/config
export default defineConfig({
  i18n: {
    defaultLocale: "en",
    locales: ["en", "pl"],
    routing: {
      prefixDefaultLocale: false,
      redirectToDefaultLocale: true
    },
    fallback: {
      pl: "en"
    }
  },
  base: 'portfolio',
  site: 'https://truetimewatcher.gitlab.io/portfolio/',
  outDir: 'public',
  publicDir: 'static',
  integrations: [mdx(), sitemap()]
});