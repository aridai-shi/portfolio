---
title: "Capital M Modern"
tags: ["Showcase","UI"]
description: "A sleek modern landing page, complete with dark mode."
short: true
---